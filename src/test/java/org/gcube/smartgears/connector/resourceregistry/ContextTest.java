/**
 * 
 */
package org.gcube.smartgears.connector.resourceregistry;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.CredentialSecret;
import org.gcube.common.security.secrets.Secret;
import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextTest {
	
	protected static final String CONFIG_INI_FILENAME = "config.ini";
	
	public static final String ROOT;
	public static final String VO;
	public static final String VRE;
		
	protected static final Properties properties;
	
	protected static final String CLIENT_ID_PROPERTY_KEY = "client_id"; 
	protected static final String CLIENT_SECRET_PROPERTY_KEY = "client_secret";
	
	protected static final String clientID; 
	protected static final String clientSecret;
	
	static {
		ROOT = "/gcube";
		VO = ROOT + "/devsec";
		VRE = VO + "/devVRE";
		// VO = ROOT + "/devNext";
		// VRE = VO + "/NextNext";
		
		
		
		properties = new Properties();
		InputStream input = ContextTest.class.getClassLoader().getResourceAsStream(CONFIG_INI_FILENAME);
		try {
			// load the properties file
			properties.load(input);
			
			clientID = properties.getProperty(CLIENT_ID_PROPERTY_KEY);
			clientSecret = properties.getProperty(CLIENT_SECRET_PROPERTY_KEY);
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	public static void set(Secret secret) throws Exception {
		SecretManagerProvider.reset();
		SecretManagerProvider.set(secret);
	}
	
	public static void setContextByName(String fullContextName) throws Exception {
		Secret secret = getSecretByContextName(fullContextName);
		set(secret);
	}
	
	
	private static Secret getSecretByContextName(String fullContextName) throws Exception {
		CredentialSecret credentialSecret = new CredentialSecret(clientID, clientSecret, fullContextName);
		return credentialSecret;
	}
	
	@BeforeClass
	public static void beforeClass() throws Exception {
		setContextByName(VRE);
	}
	
	@AfterClass
	public static void afterClass() throws Exception {
		SecretManagerProvider.reset();
	}
	
}
