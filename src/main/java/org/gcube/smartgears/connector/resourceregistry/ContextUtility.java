package org.gcube.smartgears.connector.resourceregistry;

import java.util.UUID;

import org.gcube.informationsystem.resourceregistry.api.contexts.ContextCache;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
public class ContextUtility {

	public static UUID getContextUUID(String contextFullName) throws ResourceRegistryException {
		ContextCache contextCache = ContextCache.getInstance();
		UUID contextUUID = contextCache.getUUIDByFullName(contextFullName);
		return contextUUID;
	}

}
