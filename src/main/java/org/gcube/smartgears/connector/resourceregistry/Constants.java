package org.gcube.smartgears.connector.resourceregistry;

import org.gcube.smartgears.connector.resourceregistry.resourcemanager.EServiceManager;
import org.gcube.smartgears.connector.resourceregistry.resourcemanager.HostingNodeManager;

/**
 * Library-wide constants.
 * @author Luca Frosini
 * @author Lucio Lelii
 */
public class Constants {

	public static final String HOSTING_NODE_MANAGER_PROPERTY = HostingNodeManager.class.getSimpleName();
	
	public static final String ESERVICE_MANAGER_PROPERTY = EServiceManager.class.getSimpleName();
	
}
