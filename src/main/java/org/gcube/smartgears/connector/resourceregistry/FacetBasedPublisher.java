package org.gcube.smartgears.connector.resourceregistry;

import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

import org.gcube.common.security.AuthorizedTasks;
import org.gcube.common.security.factories.AuthorizationProvider;
import org.gcube.common.security.factories.AuthorizationProviderFactory;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.publisher.ResourceRegistryPublisher;
import org.gcube.informationsystem.resourceregistry.publisher.ResourceRegistryPublisherFactory;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;
import org.gcube.smartgears.configuration.AuthorizationProviderConfiguration;
import org.gcube.smartgears.connector.resourceregistry.resourcemanager.EServiceManager;
import org.gcube.smartgears.connector.resourceregistry.resourcemanager.HostingNodeManager;
import org.gcube.smartgears.context.Property;
import org.gcube.smartgears.context.application.ApplicationContext;
import org.gcube.smartgears.context.container.ContainerContext;
import org.gcube.smartgears.publishing.Publisher;
import org.gcube.smartgears.publishing.SmartgearsProfilePublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SmartgearsProfilePublisher
public class FacetBasedPublisher implements Publisher {

	private static Logger logger = LoggerFactory.getLogger(FacetBasedPublisher.class);
	
	public static AuthorizationProvider getAuthorizationProvider(ApplicationContext applicationContext) {
		return FacetBasedPublisher.getAuthorizationProvider(applicationContext.container());
	}
	
	public static AuthorizationProvider getAuthorizationProvider(ContainerContext containerContext) {
		AuthorizationProviderConfiguration authorizationProviderConfiguration = containerContext.configuration().authorizationConfiguration();
		AuthorizationProviderFactory<?> authorizationProviderFactory = authorizationProviderConfiguration.getAuthProviderFactory();
		AuthorizationProvider authorizationProvider = authorizationProviderFactory.connect(authorizationProviderConfiguration.getCredentials());
		return authorizationProvider;
	}
	
	protected ResourceRegistryPublisher resourceRegistryPublisher;
	
	@Override
	public boolean create(ContainerContext containerContext, Set<String> contexts) {
		AuthorizationProvider authorizationProvider = FacetBasedPublisher.getAuthorizationProvider(containerContext);
		SortedSet<String> contextToIterate = new TreeSet<>(contexts);
		final String context = contextToIterate.first();
		try {
			AuthorizedTasks.executeSafely( () -> {
				if(resourceRegistryPublisher==null) {
					resourceRegistryPublisher = ResourceRegistryPublisherFactory.create();
				}
				HostingNodeManager hostingNodeManager = null;
				for (final String c : contextToIterate) {
					try {
						if(context.compareTo(c)==0) {
							hostingNodeManager = new HostingNodeManager(containerContext, resourceRegistryPublisher);
							hostingNodeManager.createHostingNode();
							Property property = new Property(Constants.HOSTING_NODE_MANAGER_PROPERTY, hostingNodeManager);
							containerContext.properties().add(property);
						}else {
							UUID contextUUID = ContextUtility.getContextUUID(context);
							hostingNodeManager.addToContext(contextUUID, context);
						}
					}catch (Throwable e) {
						logger.error("Error while publishing {} (id='{}') in context '{}'", HostingNode.NAME, containerContext.id(), context, e);
					}
				}
			}, authorizationProvider.getSecretForContext(context));
		}catch (Throwable e) {
			logger.error("Error while publishing {} (id='{}')", HostingNode.NAME, containerContext.id(), e);
		}
		return true;
	}

	@Override
	public boolean create(ApplicationContext applicationContext, Set<String> contexts) {
		AuthorizationProvider authorizationProvider = FacetBasedPublisher.getAuthorizationProvider(applicationContext);
		SortedSet<String> contextToIterate = new TreeSet<>(contexts);
		final String context = contextToIterate.first();
		
		try {
			AuthorizedTasks.executeSafely( () -> {
				if(resourceRegistryPublisher==null) {
					resourceRegistryPublisher = ResourceRegistryPublisherFactory.create();
				}
				EServiceManager eServiceManager = null;
				for (final String c : contextToIterate) {
					try {
						if(context.compareTo(c)==0) {
							eServiceManager = new EServiceManager(applicationContext, resourceRegistryPublisher);
							eServiceManager.createEService();
							Property property = new Property(Constants.ESERVICE_MANAGER_PROPERTY, eServiceManager);
							applicationContext.properties().add(property);
						}else {
							UUID contextUUID = ContextUtility.getContextUUID(context);
							eServiceManager.addToContext(contextUUID, context);
						}
					}catch (Throwable e) {
						logger.error("Error while publishing {} (name='{}', id='{}') in context '{}'", EService.NAME, applicationContext.name(), applicationContext.id(), context, e);
					}
				}
			}, authorizationProvider.getSecretForContext(context));
		}catch (Throwable e) {
			logger.error("Error while publishing {} (name='{}', id='{}')", EService.NAME, applicationContext.name(), applicationContext.id(), e);
		}
		return true;
	}

	@Override
	public boolean remove(ContainerContext containerContext, Set<String> contexts) {
		HostingNodeManager hostingNodeManager = (HostingNodeManager) containerContext.properties().lookup(Constants.HOSTING_NODE_MANAGER_PROPERTY).value();
		AuthorizationProvider authorizationProvider = FacetBasedPublisher.getAuthorizationProvider(containerContext);
		for(String context : contexts) {
			try {
				AuthorizedTasks.executeSafely( () -> {
					try {
						hostingNodeManager.removeFromCurrentContext();
					}catch (Exception e) {
						throw new RuntimeException(e);
					}
	
				}, authorizationProvider.getSecretForContext(context));
			}catch (Throwable e) {
				logger.error("Unable to remove {} (id='{}') from context '{}'", HostingNode.NAME, containerContext.id(), context, e);
			}
		}
		return false;
	}

	@Override
	public boolean remove(ApplicationContext applicationContext, Set<String> contexts) {
		EServiceManager eServiceManager = (EServiceManager) applicationContext.properties().lookup(Constants.ESERVICE_MANAGER_PROPERTY).value();
		AuthorizationProvider authorizationProvider = FacetBasedPublisher.getAuthorizationProvider(applicationContext);
		for(String context : contexts) {
			try {
				AuthorizedTasks.executeSafely( () -> {
					try {
						eServiceManager.removeFromCurrentContext();
					}catch (Exception e) {
						throw new RuntimeException(e);
					}
	
				}, authorizationProvider.getSecretForContext(context));
			}catch (Throwable e) {
				logger.error("Unable to remove {} (name='{}', id='{}') from context '{}'", EService.NAME, applicationContext.name(), applicationContext.id(), context, e);
			}
		}
		return true;
	}
	
	@Override
	public boolean update(ContainerContext containerContext) {
		HostingNodeManager hostingNodeManager = (HostingNodeManager) containerContext.properties().lookup(Constants.HOSTING_NODE_MANAGER_PROPERTY).value();
		AuthorizationProvider authorizationProvider = FacetBasedPublisher.getAuthorizationProvider(containerContext);
		SortedSet<String> contextToIterate = new TreeSet<>(authorizationProvider.getContexts());
		final String context = contextToIterate.first();
		try {
			AuthorizedTasks.executeSafely( () -> {
				try {
					hostingNodeManager.updateFacets();
				} catch (ResourceRegistryException e) {
					throw new RuntimeException(e);
				}
			}, authorizationProvider.getSecretForContext(context));
		}catch (Throwable e) {
			logger.error("Unable to update {} (id='{}')", HostingNode.NAME, containerContext.id(), e);
		}
		return true;
	}
	
	@Override
	public boolean update(ApplicationContext applicationContext) {
		EServiceManager eServiceManager = (EServiceManager) applicationContext.properties().lookup(Constants.ESERVICE_MANAGER_PROPERTY).value();
		AuthorizationProvider authorizationProvider = FacetBasedPublisher.getAuthorizationProvider(applicationContext);
		SortedSet<String> contextToIterate = new TreeSet<>(authorizationProvider.getContexts());
		final String context = contextToIterate.first();
		try {
			AuthorizedTasks.executeSafely( () -> {
				try {
					eServiceManager.updateFacets();
				} catch (ResourceRegistryException e) {
					throw new RuntimeException(e);
				}
			}, authorizationProvider.getSecretForContext(context));
		}catch (Throwable e) {
			logger.error("Unable to update {} (name='{}', id='{}') ", EService.NAME, applicationContext.name(), applicationContext.id(), e);
		}
		return true;
	}
	
}
